package nl.utwente.di.tempTranslate;

public class Translate {

    public double celsiusToFahrenheit(Double celsius) {
        return celsius * (9.00/5.00) + 32;
    }
}
