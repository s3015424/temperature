package nl.utwente.di.tempTranslate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/** * Tests the Quoter */
public class TestQuoter {
    @Test
    public void testBook1() throws Exception {
        Translate translate = new Translate();
        double fahrenheit = translate.celsiusToFahrenheit(2.00);
        Assertions.assertEquals(35.6, fahrenheit, 0.0, "Price of book 1");
    }
}